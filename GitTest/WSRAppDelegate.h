//
//  WSRAppDelegate.h
//  GitTest
//
//  Created by Greg wsr on 8/20/14.
//  Copyright (c) 2014 Greg wsr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WSRAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
