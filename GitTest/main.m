//
//  main.m
//  GitTest
//
//  Created by Greg wsr on 8/20/14.
//  Copyright (c) 2014 Greg wsr. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WSRAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WSRAppDelegate class]));
    }
}
