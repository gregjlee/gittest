//
//  WSRDetailViewController.h
//  GitTest
//
//  Created by Greg wsr on 8/20/14.
//  Copyright (c) 2014 Greg wsr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WSRDetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
